/* 
    Fetch Method

    - The Fetch method in JavaScript is used to request data from a server. The request can be of any type of API that returns the data in JSON or XML format.

    Sample fetching:
    fetch('https://jsonplaceholder.typicode.com/posts') - api for the get request


    The fetch() method takes one mandatory argument, the path to the resource you want to fetch.
    It returns a promise that resolves to the Response to that request, whether it is successful or not.
    The promise will reject with a TypeError when a network error is encountered or CORS is misconfigured on the server-side, although this usually means permission issues or similar — a 404 does not constitute a network error, for example.
    By default, fetch won't send or receive any cookies from the server, resulting in unauthenticated requests if the site relies on maintaining a user session (to send cookies, the credentials init option must be set).
*/

// Fetching the data from the api
// GET post data from the api

fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => response.json())
    .then((data) => showPosts(data))



// Add post data to the api
// making post request using fetch api: Post request can be made using fetch by giving options as given below

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;

	})
})


// show posts


const showPosts = (posts) => {
    let postEntries = '';
    
    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `;
    });
document.querySelector('#div-post-entries').innerHTML = postEntries;
}



// edit post
// when you click the edit button the value will be go to form-edit-post

const editPost =  (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-title-${id}`).innerHTML

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

    // removeAttribute() method removes the "disabled" attribute from the selected element
    // this will automatically enable the update button once edit button is pressed
    document.querySelector('#btn-submit-update').removeAttribute('disabled');

}


// update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: "PUT",
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),

        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })

    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Successfully updated')

    document.querySelector('#txt-edit-id').value = null;
    document.querySelector('#txt-edit-title').value = null;
    document.querySelector('#txt-edit-body').value = null;

    // this code could be used to disable a submit button on a form, preventing the user from interacting with the button and submitting the form after sending the update

    // when a submit button is disabled, it appears grayed out and cannot be click
    document.querySelector('#btn-submit-update').setAttribute('disabled', true);

    });  
});


// ========= Activity =======

// delete post

const deletePost = (id) => {

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'DELETE'
    });

    document.querySelector(`#post-${id}`).remove();
}